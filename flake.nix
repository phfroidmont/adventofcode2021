{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        millWithJdk11 = pkgs.mill.override { jre = pkgs.jdk11; };

        inputs = with pkgs; [
          millWithJdk11
        ];
      in
      {
        devShell = pkgs.mkShell {
          buildInputs = inputs;
          shellHook = ''
            set -a
            JAVA_HOME=${pkgs.jdk11}
            set +a
          '';
        };
      }
    );
}
