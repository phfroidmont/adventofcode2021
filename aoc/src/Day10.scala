import scala.io.Source
import scala.util.Try

object Day10 extends App:

  val input = Source
    .fromURL(getClass.getResource("day10Input.txt"))
    .mkString

  enum ChunkType(val openChar: Char, val closeChar: Char):
    case Parens extends ChunkType('(', ')')
    case ABrackets extends ChunkType('<', '>')
    case Braces extends ChunkType('{', '}')
    case SBrackets extends ChunkType('[', ']')

  def parseChunk(line: String): Either[Char, Seq[ChunkType]] =
    line.toList
      .foldLeft[Either[Char, Seq[ChunkType]]](Right(Seq())) {
        case (Right(chunks), char) =>
          ChunkType.values
            .find(_.openChar == char)
            .map(chunk => Right(chunk +: chunks))
            .getOrElse {
              ChunkType.values
                .find(_.closeChar == char)
                .filter(_ == chunks.head)
                .map(_ => Right(chunks.tail))
                .getOrElse(Left(char))

            }
        case (Left(c), _) => Left(c)
      }

  val invalidCharToPoints: Char => Int = {
    case ')' => 3
    case ']' => 57
    case '}' => 1197
    case '>' => 25137
    case _   => 0
  }

  val part1 = input
    .split('\n')
    .map(parseChunk)
    .flatMap {
      case Right(_)   => None
      case Left(char) => Some(char)
    }
    .map(invalidCharToPoints)
    .sum

  println(s"Part 1: ${part1}")

  val charCompletionToPoints: ChunkType => Long = {
    case ChunkType.Parens    => 1
    case ChunkType.SBrackets => 2
    case ChunkType.Braces    => 3
    case ChunkType.ABrackets => 4
  }

  val part2Sorted = input
    .split('\n')
    .toList
    .flatMap(parseChunk(_).toOption)
    .map(_.map(charCompletionToPoints).fold(0L)((a, b) => a * 5 + b))
    .sorted

  val part2Middle = part2Sorted.drop(part2Sorted.size / 2).head

  println(s"Part 2: ${part2Middle}")
