import scala.io.Source
object Day1 extends App:

  val mesurements = Source
    .fromURL(getClass.getResource("day1Input.txt"))
    .mkString
    .split('\n')
    .map(_.toInt)
    .toList

  val total = mesurements.zip(mesurements.drop(1)).map(_ < _).count(identity)

  println(s"Part 1: $total")

  val windows = mesurements
    .zip(mesurements.drop(1))
    .zip(mesurements.drop(2))
    .map { case ((m1, m2), m3) =>
      m1 + m2 + m3
    }

  val windowsTotal = windows.zip(windows.drop(1)).map(_ < _).count(identity)

  println(s"Part 2: $windowsTotal")
