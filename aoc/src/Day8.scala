import scala.io.Source

object Day8 extends App:

  val input = Source
    .fromURL(getClass.getResource("day8Input.txt"))
    .mkString

  val easyToIdentifyCount =
    input
      .split('\n')
      .toList
      .flatMap(
        _.split(" \\| ").last
          .split(' ')
          .toList
          .map(_.size)
      )
      .count(size => size == 2 || size == 3 || size == 4 || size == 7)

  println(s"Part 1: ${easyToIdentifyCount}")

  enum Letter:
    case A, B, C, D, E, F, G
  object Letter:
    def parse(input: Char): Letter = input match
      case 'a' => A
      case 'b' => B
      case 'c' => C
      case 'd' => D
      case 'e' => E
      case 'f' => F
      case 'g' => G

  final case class Signal(letters: Set[Letter]):
    import Letter._
    def computeDigit(segments: Map[Letter, Letter]): Int =
      val actualSegments = letters.map(segments)
      if (actualSegments == Set(A, B, C, E, F, G)) 0
      else if (actualSegments == Set(C, F)) 1
      else if (actualSegments == Set(A, C, D, E, G)) 2
      else if (actualSegments == Set(A, C, D, F, G)) 3
      else if (actualSegments == Set(B, C, D, F)) 4
      else if (actualSegments == Set(A, B, D, F, G)) 5
      else if (actualSegments == Set(A, B, D, E, F, G)) 6
      else if (actualSegments == Set(A, C, F)) 7
      else if (actualSegments == Set(A, B, C, D, E, F, G)) 8
      else 9
  object Signal:
    def parse(input: String): Signal =
      Signal(
        input.toSet
          .map(Letter.parse)
      )

  final case class Entry(signals: Seq[Signal], output: Seq[Signal]):
    import Letter._
    private def findSegments(): Map[Letter, Letter] =
      val one = signals.find(_.letters.size == 2).get
      val four = signals.find(_.letters.size == 4).get
      val seven = signals.find(_.letters.size == 3).get
      val six = signals
        .filter(_.letters.size == 6)
        .filter(signal => (signal.letters -- one.letters).size == 5)
        .head
      val zero = signals
        .filter(signal =>
          signal.letters.size == 6 && signal.letters != six.letters
        )
        .filter(signal => (signal.letters -- four.letters).size == 3)
        .head
      val nine = signals
        .filter(signal =>
          signal.letters.size == 6 && signal.letters != six.letters && signal.letters != zero.letters
        )
        .head
      val eight = signals.find(_.letters.size == 7).get

      val a = (seven.letters -- one.letters).head
      val c = (one.letters -- six.letters).head
      val d = (eight.letters -- zero.letters).head
      val e = (eight.letters -- nine.letters).head
      val f = (one.letters - c).head
      val b = (four.letters - c - d - f).head
      val g = (nine.letters -- four.letters - a).head

      Map(
        (a, A),
        (b, B),
        (c, C),
        (d, D),
        (e, E),
        (f, F),
        (g, G)
      )

    def computeOutputValue(): Int =
      val segments = findSegments()
      val digits = output.map(_.computeDigit(segments))
      digits(0) * 1000 +
        digits(1) * 100 +
        digits(2) * 10 +
        digits(3)

  object Entry:
    def parse(input: String): Entry =
      val splitedLine = input.split(" \\| ")
      Entry(
        splitedLine.head.split(' ').map(Signal.parse),
        splitedLine.last.split(' ').map(Signal.parse)
      )

  val outputs =
    input
      .split('\n')
      .toList
      .map(line => Entry.parse(line))
      .map(_.computeOutputValue())

  println(s"Part 2: ${outputs.sum}")
