import scala.io.Source
import scala.util.Try

object Day9 extends App:

  val input = Source
    .fromURL(getClass.getResource("day9Input.txt"))
    .mkString

  final case class HeightMap(heights: Array[Array[Int]]):
    def findNeighbours(x: Int, y: Int): Set[(Int, Int)] =
      Set((x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1))
        .filter((x, y) => Try(heights(y)(x)).isSuccess)

    def findLowPoints(): Seq[(Int, Int)] =
      for
        y <- (0 until heights.length)
        x <- (0 until heights(0).length)
        height = heights(y)(x)
        neighbours = findNeighbours(x, y)
        if neighbours.forall(it => heights(it._2)(it._1) > height)
      yield (x, y)

    def extendBasin(basin: Set[(Int, Int)]): Set[(Int, Int)] =
      val extendedBasin: Set[(Int, Int)] =
        for
          (x, y) <- basin
          height = heights(y)(x)
          neighbour <- findNeighbours(x, y).filter(it =>
            heights(it._2)(it._1) > height && heights(it._2)(it._1) < 9
          ) + ((x, y))
        yield neighbour

      if (basin == extendedBasin)
        extendedBasin
      else extendBasin(extendedBasin)

  object HeightMap:
    def parse(input: String): HeightMap =
      HeightMap(
        input
          .split('\n')
          .map(_.toArray.map(_.asDigit))
      )

  val heightMap = HeightMap.parse(input)

  val part1 =
    heightMap.findLowPoints().map(it => heightMap.heights(it._2)(it._1) + 1).sum

  println(s"Part 1: ${part1}")

  val part2 =
    heightMap
      .findLowPoints()
      .toList
      .map(it => heightMap.extendBasin(Set(it)))
      .map(_.size)
      .sorted
      .reverse
      .take(3)
      .reduce(_ * _)

  println(s"Part 2: ${part2}")
