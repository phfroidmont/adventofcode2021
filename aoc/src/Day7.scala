import scala.io.Source

object Day7 extends App:

  val input = Source
    .fromURL(getClass.getResource("day7Input.txt"))
    .mkString

  val crabPositions = input.split(',').map(_.toInt).toSeq.sorted

  val fuelConsumptions =
    for possiblePostion <- (crabPositions.head to crabPositions.last)
    yield crabPositions.map(_ - possiblePostion).map(_.abs).sum

  println(s"Part 1: ${fuelConsumptions.sorted.head}")

  val fuelConsumptionsPart2 =
    for possiblePostion <- (crabPositions.head to crabPositions.last)
    yield crabPositions
      .map(_ - possiblePostion)
      .map(_.abs)
      .map(distance => (1 to distance).sum)
      .sum
  println(s"Part 2: ${fuelConsumptionsPart2.sorted.head}")
