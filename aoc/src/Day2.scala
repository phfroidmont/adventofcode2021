import scala.io.Source
object Day2 extends App:
  enum Move:
    case Forward(distance: Int) extends Move
    case Down(distance: Int) extends Move
    case Up(distance: Int) extends Move

  import Move._

  val moves = Source
    .fromURL(getClass.getResource("day2Input.txt"))
    .mkString
    .split('\n')
    .toList
    .map(_.split(' '))
    .map {
      case Array("forward", distance) => Forward(distance.toInt)
      case Array("down", distance)    => Down(distance.toInt)
      case Array("up", distance)      => Up(distance.toInt)
    }

  case class SimplePosition(horizontal: Int, depth: Int) {
    def performMove(move: Move): SimplePosition = move match {
      case Forward(distance) =>
        SimplePosition(horizontal + distance, depth)
      case Up(distance) =>
        SimplePosition(horizontal, depth - distance)
      case Down(distance) =>
        SimplePosition(horizontal, depth + distance)
    }
  }

  val finalPositionPart1 =
    moves.foldLeft(SimplePosition(0, 0))(_.performMove(_))
  println(
    s"Part 1: ${finalPositionPart1.depth * finalPositionPart1.horizontal}"
  )

  case class BetterPosition(aim: Int, horizontal: Int, depth: Int) {
    def performMove(move: Move): BetterPosition = move match {
      case Forward(distance) =>
        BetterPosition(aim, horizontal + distance, depth + aim * distance)
      case Up(distance) =>
        this.copy(aim - distance)
      case Down(distance) =>
        this.copy(aim + distance)
    }
  }

  val finalPositionPart2 =
    moves.foldLeft(BetterPosition(0, 0, 0))(_.performMove(_))
  println(
    s"Part 2: ${finalPositionPart2.depth * finalPositionPart2.horizontal}"
  )
